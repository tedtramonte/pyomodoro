__title__ = 'pyomodoro'
__author__ = 'Ted Tramonte'
__license__ = 'MIT'
__copyright__ = 'Copyright 2020 Ted Tramonte'

from .cli import start  # noqa
